import React, { useReducer } from "react";
import { NextPage } from "next";
type State = { loading: boolean };
type Action = { type: "startLoading" } | { type: "endLoading" };
type Dispatch = (action: Action) => void;

export const LoadingContext =
  React.createContext<{ state: State; dispatch: Dispatch } | undefined>(
    undefined
  );

const initialState = {
  loading: true,
};

const { Provider } = LoadingContext;

const reducer = (state: State, action: Action) => {
  switch (action.type) {
    case "startLoading":
      return {
        loading: true,
      };
    case "endLoading":
      return {
        loading: false,
      };
    default:
      throw state;
  }
};

const LoadingProvider: NextPage = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const value = { state, dispatch };
  return <Provider value={value}>{children}</Provider>;
};

export function useLoading() {
  const context = React.useContext(LoadingContext);
  if (context === undefined) {
    throw new Error("useCount must be used within a CountProvider");
  }
  return context;
}

export default LoadingProvider;
