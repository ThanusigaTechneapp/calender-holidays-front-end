import type { GetServerSideProps, NextPage } from "next";
import dynamic from "next/dynamic";
import { useEffect } from "react";
import styles from "../styles/Home.module.css";
import { GetAllHolidays } from "../services/Be-Api";
import { GetAllHolidaysData, Holidays } from "../interfaces/GetAllHolidaysData";

const CalenderHolidays = dynamic(
  () => import("../components/calender-holidays/Calender-Holidays"),
  // eslint-disable-next-line
  { loading: () => <p className={styles.loading}>Loading ...</p>, ssr: false }
);

const Home: NextPage<{ holidays: GetAllHolidaysData }> = ({ holidays }) => {
  return (
    <div>
      {/* <CalenderHolidays /> */}
      <CalenderHolidays holidays={holidays} />
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  let country = "ro";
  let year = 2021;
  let holidays: GetAllHolidaysData;

  try {
    const { data } = await GetAllHolidays(country, year);
    holidays = data;

    console.log("hii");
    console.log(holidays);

    // Object.prototype(data.holidays)

    // Object.keys(data).map(function (key, index) {
    //   const incomingCountry = data[key];

    // });

    // console.log("incoming country " + incomingCountry);
  } catch (error) {
    console.log(error);
  }

  return {
    props: {
      holidays,
    },
  };
};

export default Home;
