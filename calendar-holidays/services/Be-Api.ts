import axios from "axios";
import { GetAllHolidaysData } from "../interfaces/GetAllHolidaysData";

process.env.NODE_TLS_REJECT_UNAUTHORIZED;
const http = axios.create({ baseURL: process.env.NEXT_PUBLIC_API });

export const GetAllHolidays = (country: string, year: number) => {
  return http.get<GetAllHolidaysData>(
    `/holiday?&country=${country}&year=${year}`
  );
};
