export interface GetAllHolidaysData {
  version: number;
  billing: Billing;
  holidays: Holidays[];
}

export interface Billing {
  credits: number;
}

export interface Holidays {
  id: number;
  urlid: string;
  url: string;
  country: Country;
  name: Name[];
  oneliner: Oneliner[];
  type: string[];
  subtype: Object;
  date: Date;
  uid: string;
}

export interface Country {
  id: string;
  name: string;
}

export interface Name {
  lang: string;
  text: string;
}
export interface Oneliner {
  lang: string;
  text: string;
}
export interface Date {
  iso: string;
  datetime: Datetime;
}
export interface Datetime {
  year: number;
  month: number;
  day: number;
}
