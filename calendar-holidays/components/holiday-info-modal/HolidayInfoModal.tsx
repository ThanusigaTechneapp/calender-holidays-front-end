import { NextPage } from "next";
import styles from "./HolidayInfo.module.scss";

const HolidayInfo: NextPage<{
  closeHolidayModal;
  selectedItem;
  getMonthText;
  // country;
  // holiday_type_unique;
}> = ({ closeHolidayModal, selectedItem, getMonthText }) => {
  const holidayName = selectedItem.name.map((name) => name.text);
  const countryName = selectedItem.country.name;
  const hlidayType = selectedItem.types;
  const description = selectedItem.oneliner.map((holiday) => holiday.text);
  const dateOfHoliday = selectedItem.date.iso;
  const timeAndDateURL = selectedItem.url;
  const monthInText = getMonthText(selectedItem.date.iso.split("-")[1]);
  const year = selectedItem.date.datetime.year;
  const day = selectedItem.date.datetime.day;
  return (
    <>
      {/* <div className={styles.holiday_info_background}></div> */}
      <div className={styles.holiday_info_wrapper}>
        <div className={styles.close_btn_block}>
          <button onClick={closeHolidayModal}>
            <svg viewBox="0 0 24 24">
              <path d="M0 0h24v24H0V0z" fill="none" />
              <path d="M18.3 5.71c-.39-.39-1.02-.39-1.41 0L12 10.59 7.11 5.7c-.39-.39-1.02-.39-1.41 0-.39.39-.39 1.02 0 1.41L10.59 12 5.7 16.89c-.39.39-.39 1.02 0 1.41.39.39 1.02.39 1.41 0L12 13.41l4.89 4.89c.39.39 1.02.39 1.41 0 .39-.39.39-1.02 0-1.41L13.41 12l4.89-4.89c.38-.38.38-1.02 0-1.4z" />
            </svg>
          </button>
        </div>
        <h2>
          {holidayName} in {countryName}
        </h2>
        <div className={styles.holiday_details_wrapper}>
          <p> {hlidayType}</p>
          <p> {description}</p>
          <p> {dateOfHoliday}</p>
          <p> {timeAndDateURL}</p>
          <p> {holidayName}</p>
          <p> {year}</p>
          <p> {monthInText}</p>
          <p> {day}</p>
        </div>
      </div>
    </>
  );
};

export default HolidayInfo;
