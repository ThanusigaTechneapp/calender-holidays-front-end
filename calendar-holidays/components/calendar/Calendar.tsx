import styles from "./Calendar.module.scss";
import Calendar from "react-awesome-calendar";
import { NextPage } from "next";
import { Holidays } from "../../interfaces/GetAllHolidaysData";

const Calender: NextPage<{ calanderRef }> = (calanderRef) => {
  return (
    <>
      <Calendar ref={calanderRef} />
    </>
  );
};

Calender.displayName = "Calender";

export default Calender;

// [
//   {
//     "id": 1687,
//     "urlid": "romania/new-year-day",
//     "url": "https://www.timeanddate.com/holidays/romania/new-year-day",
//     "country": {
//       "id": "ro",
//       "name": "Romania"
//     },
//     "types": [
//       "National holiday"
//     ]
//   }
// ]

// How can i map this, `[
//   {
//     "id": 1687,
//     "urlid": "romania/new-year-day",
//     "url": "https://www.timeanddate.com/holidays/romania/new-year-day",
//     "country": {
//       "id": "ro",
//       "name": "Romania"
//     },
//     "types": [
//       "National holiday"
//     ]
//   }
// ]`   I tried with `
//   const incoming_holiday = holidays.map((item, index) => {
//     item.types;
//   });

//   console.log(incoming_holiday);`  I got error as `Cannot read property 'map' of undefined` why???
