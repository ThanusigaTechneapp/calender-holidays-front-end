import { NextPage } from "next";
import Image from "next/image";
import React, { useEffect, useLayoutEffect, useRef, useState } from "react";
import Select from "react-select";
import styles from "./Calender.module.scss";
import Year from "../data/Year";
import Country from "../data/Country";
import Month from "../data/Month";
import Grid from "@mui/material/Grid";
import Calendar from "react-awesome-calendar";
import Footer from "../footer/Footer";
import Brightness6Icon from "@mui/icons-material/Brightness6";
import HolidayInfo from "../holiday-info-modal/HolidayInfoModal";
import SadEmoji from "../../public/sad-emoji.jpeg";

interface data {
  holidays: any;
}

var today = new Date();
var dd = String(today.getDate()).padStart(2, "0");
var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
var yyyy = today.getFullYear().toString();

const currentYear = [{ value: yyyy, label: yyyy }];

const getCurrentMonth = (text: string) => {
  switch (text) {
    case "01":
      return "January";
    case "02":
      return "Febuary";
    case "03":
      return "March";
    case "04":
      return "April";
    case "05":
      return "May";
    case "06":
      return "June";
    case "07":
      return "July";
    case "08":
      return "August";
    case "09":
      return "September";
    case "10":
      return "October";
    case "11":
      return "November";
    case "12":
      return "December";
    default:
      return "";
  }
};
const currentMonth = [
  { value: getCurrentMonth(mm), label: getCurrentMonth(mm) },
];
console.log("current month", currentMonth);

const CalenderHolidays: NextPage<data> = ({ holidays }) => {
  const [selectedMonth, setselectedMonth] = useState("");
  const [selectedYear, setselectedYear] = useState(2021);
  const [selectedCountry, setselectedCountry] = useState("Sri Lanka");
  const [openModal, setOpenModal] = useState(false);
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const [theme, setTheme] = useState("light");
  const [calendarMode, setCalendarMode] =
    useState<"dailyMode" | "monthlyMode" | "yearlyMode">("yearlyMode");
  const calanderRef = useRef();

  const onCalendarChangeHanlder = (event) => {
    console.log("event", event);
    console.log(event.mode);
    // console.log(calanderRef)
    const day = event.day;
    const month = event.month;
    const year = event.year;

    console.log(day);
    console.log(month);
    console.log(year);

    if (event.mode === "monthlyMode") {
      setCalendarMode("monthlyMode");
    }
    if (event.mode === "yearlyMode") {
      setCalendarMode("yearlyMode");
    }

    if (event.mode === "dailyMode") {
      setCalendarMode("dailyMode");
    }
  };

  // useEffect(() => {
  //   // @ts-ignore
  //   calanderRef.current.onClickMode(calendarMode);

  //   if (calendarMode === "yearlyMode") {
  //     const yearlycalendar = document.getElementById("year_calendar_wrapper");
  //     yearlycalendar.style.height = "1600px";
  //   }

  //   if (calendarMode === "monthlyMode") {
  //     const monthlycalendar = document.getElementById("month_calendar_wrapper");
  //     monthlycalendar.style.height = "860px";
  //   }
  // }, [calendarMode]);

  // console.log(calanderRef.current);

  useEffect(() => {
    document.documentElement.setAttribute(
      "data-theme",
      localStorage.getItem("theme")
    );
    setTheme(localStorage.getItem("theme"));
  }, []);

  const switchTheme = () => {
    if (theme === "light") {
      saveTheme("dark");
    } else {
      saveTheme("light");
    }
  };
  const saveTheme = (theme) => {
    setTheme(theme);
    localStorage.setItem("theme", theme);
    document.documentElement.setAttribute("data-theme", theme);
  };

  const onMenuOpen = () => setIsMenuOpen(true);
  const onMenuClose = () => setIsMenuOpen(false);
  const onChange = () => {
    // console.log()
  };
  const openHolidayModal = () => {
    setOpenModal(true);
  };
  const closeHolidayModal = () => {
    setOpenModal(false);
  };

  const yearChangeHanlder = (e) => {
    console.log(e);
    setselectedYear(e);
    console.log(typeof e);
  };
  const monthChangeHanlder = (e) => {
    console.log(e);
    setselectedMonth(e);
  };
  const countryChangeHanlder = (e) => {
    console.log(e);
    setselectedCountry(e);
  };
  /////////////////////////////////////
  function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }

  let holiday_details = []; // array of obejcts
  let holiday_type = []; // array of array
  let holiday_type_unique = []; // array of unique holiday type
  // var holiday_date ;   // holiday date

  const incoming_holiday = holidays.map((item, index) => {
    holiday_details.push(item);
    holiday_type.push(item.types);
    // console.log(item);
  });

  var all_holiday_types_one_arr = [].concat.apply([], holiday_type); // array of all (duplicate) holiday types

  holiday_type_unique = all_holiday_types_one_arr.filter(onlyUnique); // array of unique holiday type

  console.log(holiday_type_unique);

  const getMonthText = (text: string) => {
    switch (text) {
      case "01":
        return "January";
      case "02":
        return "Febuary";
      case "03":
        return "March";
      case "04":
        return "April";
      case "05":
        return "May";
      case "06":
        return "June";
      case "07":
        return "July";
      case "08":
        return "August";
      case "09":
        return "September";
      case "10":
        return "October";
      case "11":
        return "November";
      case "12":
        return "December";
      default:
        return "";
    }
  };
  ///////////////////////////////////////

  return (
    <div>
      <nav className={styles.nav}>
        <Grid
          container
          spacing={{ xs: 1, md: 5 }}
          className={styles.nav_items_wrapper}
        >
          <Grid className={styles.nav_text_wrapper}>Create Calendar</Grid>
          <Grid className={styles.year_wrapper}>
            <span className={styles.nav_text_wrapper}>Year: </span>
            <Select
              inputId="year-input"
              onMenuOpen={onMenuOpen}
              onMenuClose={onMenuClose}
              options={Year}
              defaultValue={currentYear}
              placeholder="Select a year"
              onChange={(e) => yearChangeHanlder(e.value as any)}
              className={styles.select_year_button}
            />
          </Grid>
          {calendarMode === "monthlyMode" && (
            <Grid className={styles.year_wrapper}>
              <span className={styles.nav_text_wrapper}>Month: </span>
              <Select
                //  value={selectedMonth}
                inputId="year-input"
                onMenuOpen={onMenuOpen}
                onMenuClose={onMenuClose}
                options={Month}
                defaultValue={currentMonth}
                placeholder="Select a Month"
                onChange={(e) => monthChangeHanlder(e.value as any)}
                className={styles.select_year_button}
              />
            </Grid>
          )}
          <Grid className={styles.country_wrapper}>
            <span className={styles.nav_text_wrapper}>Country: </span>
            <Select
              //  value={selectedCountry}
              inputId="year-input"
              onMenuOpen={onMenuOpen}
              onMenuClose={onMenuClose}
              options={Country}
              defaultValue={Country[0]}
              placeholder="Select a country"
              onChange={(e) => countryChangeHanlder(e.value as any)}
              className={styles.select_country_button}
            />
          </Grid>
          <Grid className={styles.theme_button}>
            <button className={styles.themeSwitcher} onClick={switchTheme}>
              <Brightness6Icon fontSize="large" />
            </button>
          </Grid>
        </Grid>
      </nav>

      <div>
        {holidays.map((item, index) => {
          return (
            <div>
              <h1> {item.id} </h1>
              <h1> {item.types} </h1>
              <h1> {item.country.name} </h1>
              <h1> {item.date.iso} </h1>
              <h1> {item.url} </h1>
            </div>
          );
        })}
      </div>
      <div className={styles.holiday_list}>
        <h5> Holiday types</h5>
        {holiday_type_unique.map((type) => (
          <li> {type} </li>
        ))}
        {/* {holiday_type.map((name) => (
          <li>
            <span>{name[0]} </span>
            {name[1] && <span> and {name[1]} </span>}
          </li>
        ))} */}
        <h4> Holiday details</h4>
        {holiday_details.map((item) => (
          <li>
            {/* <span>{item.date.iso}---------</span> */}
            {item.date.iso.split("-")[2]} -------
            {getMonthText(item.date.iso.split("-")[1]).slice(0, 3)}
            {/* <span>{item.types}------------------</span> */}
            <span>{item.name.map((name) => name.text)}</span>
          </li>
        ))}
      </div>

      {/* {calendarMode === "dailyMode" && (
        <div
          className={styles.daily_calendar_wrapper}
          id="daily_calendar_wrapper"
        >
          <div className={styles.calendar_heading}>
            Calendar for Year {selectedYear} ( {selectedCountry} )
          </div>
          <Calendar />

          <div className={styles.daily_description}>
            <span>Oops!!! It is not a holiday</span>
            <Image src={SadEmoji} height={40} width={40} />
          </div>
        </div>
      )}
      {calendarMode === "monthlyMode" && (
        <div
          className={styles.month_calendar_wrapper}
          id="month_calendar_wrapper"
        >
          <div className={styles.calendar_heading}>
            Calendar for Year {selectedYear} ( {selectedCountry} )
          </div>
          <Calendar />
        </div>
      )}
      {calendarMode === "yearlyMode" && (
        <div
          className={styles.year_calendar_wrapper}
          id="year_calendar_wrapper"
        >
          <div className={styles.calendar_heading}>
            hiii Calendar for Year {selectedYear} ( {selectedCountry} )
          </div>
          <Calendar
          // ref={calanderRef}
          // onChange={onCalendarChangeHanlder}
          />
        </div>
      )} */}

      {calendarMode !== "dailyMode" && (
        <div className={styles.holiday_wrapper}>
          <div className={styles.holiday_heading}>Holidays</div>
          <div className={styles.holiday_chart}>
            <div className={styles.left_wrapper}>
              {/* list of holiday */}
              <div onClick={openHolidayModal} className={styles.holiday_info}>
                <div className={styles.holiday_date}>jan 23</div>
                <div className={styles.holiday_name}>New year</div>
              </div>
              <div onClick={openHolidayModal} className={styles.holiday_info}>
                <div className={styles.holiday_date}>jan 23</div>
                <div className={styles.holiday_name}>New year</div>
              </div>
              <div onClick={openHolidayModal} className={styles.holiday_info}>
                <div className={styles.holiday_date}>jan 23</div>
                <div className={styles.holiday_name}>New year</div>
              </div>
              <div onClick={openHolidayModal} className={styles.holiday_info}>
                <div className={styles.holiday_date}>jan 23</div>
                <div className={styles.holiday_name}>New year</div>
              </div>
            </div>

            <div className={styles.right_wrapper}>
              {/* list of holiday */}
              <div onClick={openHolidayModal} className={styles.holiday_info}>
                <div className={styles.holiday_date}>jan 23</div>
                <div className={styles.holiday_name}>New year</div>
              </div>
              <div onClick={openHolidayModal} className={styles.holiday_info}>
                <div className={styles.holiday_date}>jan 23</div>
                <div className={styles.holiday_name}>New year</div>
              </div>
              <div onClick={openHolidayModal} className={styles.holiday_info}>
                <div className={styles.holiday_date}>jan 23</div>
                <div className={styles.holiday_name}>New year</div>
              </div>
              <div onClick={openHolidayModal} className={styles.holiday_info}>
                <div className={styles.holiday_date}>jan 23</div>
                <div className={styles.holiday_name}>New year</div>
              </div>
              <div onClick={openHolidayModal} className={styles.holiday_info}>
                <div className={styles.holiday_date}>jan 23</div>
                <div className={styles.holiday_name}>New year</div>
              </div>
            </div>
          </div>
        </div>
      )}
      {/* {openModal && <HolidayInfo closeHolidayModal={closeHolidayModal} />} */}

      <Footer />
    </div>
  );
};

export default CalenderHolidays;
