import { NextPage } from "next";
import Image from "next/image";
import React, {
  useEffect,
  useRef,
  useState,
  createRef,
  useCallback,
} from "react";
import Select from "react-select";
import styles from "./Calender.module.scss";
import Year from "../data/Year";
import Country from "../data/Country";
import Month from "../data/Month";
import Grid from "@mui/material/Grid";
import Calendar from "react-awesome-calendar";
import Footer from "../footer/Footer";
import Brightness6Icon from "@mui/icons-material/Brightness6";
import HolidayInfo from "../holiday-info-modal/HolidayInfoModal";
import SadEmoji from "../../public/sad-emoji.jpeg";
import { useMediaQuery } from "@material-ui/core";
import { GetAllHolidaysData } from "../../interfaces/GetAllHolidaysData";

interface data {
  holidays: any;
}

var today = new Date();
console.log(today);

var dd = String(today.getDate()).padStart(2, "0");
var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
var yyyy = today.getFullYear().toString();

const currentYear = [{ value: yyyy, label: yyyy }];

const getMonthText = (text: string) => {
  switch (text) {
    case "01":
      return "January";
    case "02":
      return "Febuary";
    case "03":
      return "March";
    case "04":
      return "April";
    case "05":
      return "May";
    case "06":
      return "June";
    case "07":
      return "July";
    case "08":
      return "August";
    case "09":
      return "September";
    case "10":
      return "October";
    case "11":
      return "November";
    case "12":
      return "December";
    default:
      return "";
  }
};
const currentMonth = [{ value: getMonthText(mm), label: getMonthText(mm) }];
console.log(currentMonth);

const CalenderHolidays: NextPage<data> = ({ holidays }) => {
  // const [year, setYear] = useState(2019);
  const [menuOpen, setMenuOpen] = useState(false);
  const [selectedMonth, setselectedMonth] = useState(currentMonth);
  const [selectedYear, setselectedYear] = useState(currentYear);
  const [selectedCountry, setselectedCountry] = useState("Sri Lanka");
  const [openModal, setOpenModal] = useState(false);
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const [theme, setTheme] = useState("light");
  const [calendarMode, setCalendarMode] =
    useState<"dailyMode" | "monthlyMode" | "yearlyMode">("yearlyMode");
  const calanderRef = useRef();
  const calanderRef1 = createRef();
  const matches = useMediaQuery("(max-width:425px)");

  /////////////////////////////////////

  const [selectedItem, setSelectedItem] = useState<
    GetAllHolidaysData["holidays"][0]
  >({
    id: 0,
    urlid: "",
    url: "",
    country: { id: "", name: "" },
    name: [{ lang: "", text: "" }],
    oneliner: [],
    type: [],
    subtype: {},
    date: { iso: "", datetime: { year: 0, month: 0, day: 0 } },
    uid: "",
  });

  const openHolidayInfoCard = useCallback(
    (
      // index: string,
      // isOpen: true,
      item?: GetAllHolidaysData["holidays"][0]
    ) => {
      setSelectedItem(item);
      setOpenModal(true);
    },
    []
  );
  const closeHolidayModal = () => {
    setOpenModal(false);
  };

  const onlyUnique = (value, index, self) => {
    return self.indexOf(value) === index;
  };

  let country;
  let holiday_details = []; // array of obejcts
  let holiday_type = []; // array of array
  let holiday_type_unique = []; // array of unique holiday type
  // var holiday_date ;   // holiday date

  const incoming_holiday = holidays.map((item, index) => {
    holiday_details.push(item);
    holiday_type.push(item.types);
    country = item.country.name;
  });
  const no_of_half_holiday_details = Math.ceil(holiday_details.length / 2);
  const First_half_holiday_details = holiday_details.slice(
    0,
    no_of_half_holiday_details
  );
  const second_half_holiday_details = holiday_details.slice(
    -no_of_half_holiday_details
  );

  var all_holiday_types_one_arr = [].concat.apply([], holiday_type); // array of all (duplicate) holiday types

  holiday_type_unique = all_holiday_types_one_arr.filter(onlyUnique); // array of unique holiday type

  console.log(holiday_type_unique);

  ///////////////////////////////////////
  const toggleMenu = () => {
    document.getElementById("dealsMenu").classList.toggle("toggle_menu");
    setMenuOpen(!menuOpen);
  };

  const onCalendarChangeHanlder = (event) => {
    console.log("jjj" + event.mode);
    // const day = event.day;
    const month = event.month;
    // const year = event.year;
    // const prev = event.prev;

    // event.year = year;

    // console.log(day);
    // console.log(month);
    // console.log(year);

    // { value: getMonthText(mm), label: getMonthText(mm) }

    // setselectedYear([{ value: String(year), label: String(year) }]);
    setselectedMonth([
      {
        value: getMonthText("0" + (month + 1)),
        label: getMonthText("0" + (month + 1)),
      },
    ]);
    // console.log(selectedYear);
    // console.log(currentYear);

    if (event.mode === "monthlyMode") {
      setCalendarMode("monthlyMode");
    }
    if (event.mode === "yearlyMode") {
      setCalendarMode("yearlyMode");
    }

    if (event.mode === "dailyMode") {
      setCalendarMode("dailyMode");
    }
  };

  useEffect(() => {
    // @ts-ignore
    calanderRef.current.onClickMode(calendarMode);
    console.log(calanderRef);
  }, [calendarMode]);

  // useEffect(() => {
  //   // @ts-ignore
  //   const details = calanderRef1.current.onClickPrev();
  //   console.log(details);
  // }, ["2020"]);

  useEffect(() => {
    document.documentElement.setAttribute(
      "data-theme",
      localStorage.getItem("theme")
    );
    setTheme(localStorage.getItem("theme"));
  }, []);

  const switchTheme = () => {
    if (theme === "light") {
      saveTheme("dark");
    } else {
      saveTheme("light");
    }
  };
  const saveTheme = (theme) => {
    setTheme(theme);
    localStorage.setItem("theme", theme);
    document.documentElement.setAttribute("data-theme", theme);
  };

  const onMenuOpen = () => setIsMenuOpen(true);
  const onMenuClose = () => setIsMenuOpen(false);
  const onChange = () => {
    // console.log()
  };

  const yearChangeHanlder = (e) => {
    console.log(e);
    // setselectedYear(e);

    setselectedYear([{ value: e, label: e }]);
    console.log("selected year ");

    console.log(selectedYear);

    // Calendar.onChange(year);
    // console.log("year");
    // console.log("mkmo" + year);
  };

  const monthChangeHanlder = (e) => {
    // console.log(e);
    // setselectedMonth(e);

    setselectedMonth([
      {
        value: getMonthText("0" + e),
        label: getMonthText("0" + e),
      },
    ]);

    console.log("selectedMonth");
    console.log(selectedMonth);
  };
  const countryChangeHanlder = (e) => {
    console.log(e);
    setselectedCountry(e);
  };

  return (
    <div>
      <nav className={styles.nav}>
        <Grid
          container
          spacing={{ xs: 1, md: 5 }}
          className={styles.nav_items_wrapper}
        >
          <Grid className={styles.nav_text_wrapper}>Create Calendar</Grid>
          <Grid className={styles.year_wrapper}>
            <span className={styles.nav_text_wrapper}>Year: </span>
            <Select
              // value={selectedYear}
              inputId="year-input"
              onMenuOpen={onMenuOpen}
              onMenuClose={onMenuClose}
              options={Year}
              defaultValue={selectedYear}
              placeholder="Select a year"
              onChange={(e) => yearChangeHanlder(e.value as any)}
              className={styles.select_year_button}
            />
          </Grid>
          {calendarMode === "monthlyMode" && (
            <Grid className={styles.month_wrapper}>
              <span className={styles.nav_text_wrapper}>Month: </span>
              <Select
                //  value={selectedMonth}
                inputId="year-input"
                onMenuOpen={onMenuOpen}
                onMenuClose={onMenuClose}
                options={Month}
                defaultValue={selectedMonth}
                placeholder="Select a Month"
                onChange={(e) => monthChangeHanlder(e.value as any)}
                className={styles.select_month_button}
              />
            </Grid>
          )}
          <Grid className={styles.country_wrapper}>
            <span className={styles.nav_text_wrapper}>Country: </span>
            <Select
              //  value={selectedCountry}
              inputId="year-input"
              onMenuOpen={onMenuOpen}
              onMenuClose={onMenuClose}
              options={Country}
              defaultValue={Country[0]}
              placeholder="Select a country"
              onChange={(e) => countryChangeHanlder(e.value as any)}
              className={styles.select_country_button}
            />
          </Grid>
          <Grid className={styles.theme_button}>
            <button className={styles.themeSwitcher} onClick={switchTheme}>
              <Brightness6Icon fontSize="large" />
            </button>
          </Grid>
        </Grid>
      </nav>
      {matches && (
        <nav className={styles.mobile_nav}>
          <div className={styles.heading_button}>
            <h1> Create You Calendar </h1>
            <div className={styles.theme_menu_button}>
              <div className={styles.theme_button}>
                <button className={styles.themeSwitcher} onClick={switchTheme}>
                  <Brightness6Icon fontSize="large" />
                </button>
              </div>
              <button
                className={styles.menu_btn_wrapper}
                id="dealsMenu"
                onClick={toggleMenu}
              >
                <div></div>
                <div></div>
                <div></div>
              </button>
            </div>
          </div>

          {menuOpen && (
            <div className={styles.mobile_nav_list}>
              <ul className={styles.ul_list_mobile_nav}>
                <li className={styles.li_list_mobile_nav}>
                  <div className={styles.year_wrapper}>
                    <span className={styles.nav_text_wrapper}>Year: </span>
                    <Select
                      value={selectedYear}
                      inputId="year-input"
                      onMenuOpen={onMenuOpen}
                      onMenuClose={onMenuClose}
                      options={Year}
                      defaultValue={selectedYear}
                      placeholder="Select a year"
                      onChange={(e) => yearChangeHanlder(e.value as any)}
                      className={styles.select_year_button}
                    />
                  </div>
                </li>

                <li className={styles.li_list_mobile_nav}>
                  <div className={styles.month_wrapper}>
                    <span className={styles.nav_text_wrapper}>Month: </span>
                    <Select
                      value={selectedMonth}
                      inputId="year-input"
                      onMenuOpen={onMenuOpen}
                      onMenuClose={onMenuClose}
                      options={Month}
                      defaultValue={selectedMonth}
                      placeholder="Select a Month"
                      onChange={(e) => monthChangeHanlder(e.value as any)}
                      className={styles.select_year_button}
                    />
                  </div>
                </li>
                <li className={styles.li_list_mobile_nav}>
                  <div className={styles.country_wrapper}>
                    <span className={styles.nav_text_wrapper}>Country: </span>
                    <Select
                      //  value={selectedCountry}
                      inputId="year-input"
                      onMenuOpen={onMenuOpen}
                      onMenuClose={onMenuClose}
                      options={Country}
                      defaultValue={Country[0]}
                      placeholder="Select a country"
                      onChange={(e) => countryChangeHanlder(e.value as any)}
                      className={styles.select_country_button}
                    />
                  </div>
                </li>
                {/* <li className={styles.li_list_mobile_nav}></li> */}
              </ul>
            </div>
          )}
        </nav>
      )}

      <div
        className={` ${
          calendarMode === "yearlyMode" && styles.year_calendar_wrapper
        } ${calendarMode === "monthlyMode" && styles.month_calendar_wrapper} ${
          calendarMode === "dailyMode" && styles.daily_calendar_wrapper
        }`}
        id="year_calendar_wrapper"
      >
        <div className={styles.calendar_heading}>
          Calendar for Year{" "}
          {selectedYear.map((item, index) => (
            <span className="input-label">{item.value}</span>
          ))}
          ({selectedCountry})
        </div>
        <Calendar
          ref={calanderRef}
          onChange={onCalendarChangeHanlder}
          // year={year}
          // onClickPrev={year}
          returnTitle={"2020"}
        />

        {calendarMode === "dailyMode" && (
          <div className={styles.daily_description}>
            <span>Oops!!! It is not a holiday</span>
            <Image src={SadEmoji} height={40} width={40} />
          </div>
        )}
      </div>

      {calendarMode !== "dailyMode" && (
        <div className={styles.holiday_wrapper}>
          <div className={styles.holiday_wrapper_2}>
            <div className={styles.holiday_heading}>Holidays</div>
            <div className={styles.holiday_chart}>
              <div className={styles.left_wrapper}>
                {/* list of holiday */}
                {First_half_holiday_details.map((item) => (
                  // <li>
                  <div
                    onClick={() => openHolidayInfoCard(item)}
                    className={styles.holiday_info}
                  >
                    <div className={styles.holiday_date}>
                      {getMonthText(item.date.iso.split("-")[1]).slice(0, 3)}{" "}
                      {item.date.iso.split("-")[2]}
                    </div>
                    <div className={styles.holiday_name}>
                      {item.name.map((name) => name.text)}
                    </div>
                  </div>
                  // </li>
                ))}
              </div>

              <div className={styles.right_wrapper}>
                {/* list of holiday */}
                {second_half_holiday_details.map((item) => (
                  // <li>
                  <div
                    onClick={() => openHolidayInfoCard(item)}
                    className={styles.holiday_info}
                  >
                    <div className={styles.holiday_date}>
                      {getMonthText(item.date.iso.split("-")[1]).slice(0, 3)}{" "}
                      {item.date.iso.split("-")[2]}
                    </div>
                    <div className={styles.holiday_name}>
                      {item.name.map((name) => name.text)}
                    </div>
                  </div>
                  // </li>
                ))}
              </div>
            </div>
          </div>
        </div>
      )}
      {openModal && (
        <HolidayInfo
          // country={country}
          // holiday_type_unique={holiday_type_unique}
          selectedItem={selectedItem}
          closeHolidayModal={closeHolidayModal}
          getMonthText={getMonthText}
        />
      )}

      <div className={styles.holiday_list}>
        {/* <h5> Holiday types</h5>
        {holiday_type_unique.map((type) => (
          <li> {type} </li>
        ))} */}

        {/* <h4> Holiday details</h4>
        {holiday_details.map((item) => (
          <li>
            <div onClick={openHolidayModal} className={styles.holiday_info}>
              <div className={styles.holiday_date}>
                {getMonthText(item.date.iso.split("-")[1]).slice(0, 3)}{" "}
                {item.date.iso.split("-")[2]}
              </div>
              <div className={styles.holiday_name}>
                {item.name.map((name) => name.text)}
              </div>
            </div>
          </li>
        ))} */}
      </div>

      <Footer />
    </div>
  );
};

export default CalenderHolidays;
