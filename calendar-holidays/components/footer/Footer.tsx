import { NextPage } from "next";
import styles from './Footer.module.scss';



const Footer: NextPage = () => {

    return(
      <>
    <footer className={styles.footer}>
    <div className="col-md-8 col-sm-6 col-xs-12">
            <p className="copyright-text">Copyright &copy; 2021 All Rights Reserved 
         <a href="#"></a>.
            </p>
          </div>
    </footer>
    
      </>
    );
}

export default Footer;