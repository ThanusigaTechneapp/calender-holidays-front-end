import { NextPage } from "next";
import styles from "./Loader.module.scss";

const Loader: NextPage = () => {
  return (
    <div className={styles.loader_wrapper}>
      <div className={styles.content}>
        <span className={styles.loading_text}>Loading...</span>
      </div>
    </div>
  );
};

export default Loader;
